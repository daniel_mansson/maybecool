﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class Countdown : MonoBehaviour
{

    public int timeLeft = 60; //Seconds Overall
    public Text countdown; //UI Text Object

    void Start()
    {
      //  StartCoroutine("LoseTime");
    //    Time.timeScale = 1; //Just making sure that the timeScale is right
    }

    void Update()
    {
        //Showing the Score on the Canvas
        if ( timeLeft < 0 ) {
           // Ha GameOver();
        }
       
    }

	public void SetTime(int timeLeft)
	{
		if (timeLeft < 0)
			timeLeft = 0;

		countdown.text = ("Time left: " + timeLeft + " s");
	}

	//Simple Coroutine
	/*IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }

    }*/
}
