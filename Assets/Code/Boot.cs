﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boot : MonoBehaviour
{
	void Start ()
	{
	}

	private void Update()
	{
		if(Input.anyKeyDown || Input.GetMouseButtonDown(0))
			Game.Instance.GoToMenu();
	}

	IEnumerator Wait()
	{
		yield return new WaitForSecondsRealtime(10f);
		Game.Instance.GoToMenu();
	}
}
