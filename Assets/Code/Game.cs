﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
	public static Game Instance { get; private set; }

	public enum GameState
	{
		Boot,
		Menu,
		Game,
        GameOver
	}

	public FaceDescription IdealFace;
	public FaceDescription Face1;
	public FaceDescription Face2;

	public GameState State { get; private set; }
	public bool InTransition { get; private set; }

    public FaceDescription GetFace1(){
        return Face1;
    }

    public FaceDescription GetFace2()
    {
        return Face2;
    }

	private void Awake()
	{
		State = GameState.Boot;

		if (Instance == null)
		{
			IdealFace = FaceDescription.GetRandom();

			Face1 = FaceDescription.GetRandomExcept(IdealFace);
			Face2 = FaceDescription.GetRandomExcept(IdealFace);

			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
	}

	public Coroutine GoToMenu()
	{
		return StartCoroutine(GoToState(GameState.Menu, 1));
    }

    public Coroutine GoToGame()
	{
		IdealFace = FaceDescription.GetRandom();

		Face1 = FaceDescription.GetRandomExcept(IdealFace);
		Face2 = FaceDescription.GetRandomExcept(IdealFace);

		return StartCoroutine(GoToState(GameState.Game, 2));
    }

    public Coroutine GoToGameOver()
    {
        return StartCoroutine(GoToState(GameState.GameOver, 3));
    }

	public IEnumerator GoToState(GameState state, int sceneid)
	{
		if (InTransition || State == state)
		{
			Debug.Log("BLOCKED! Want to go to " + state);
			yield break;
		}

		Debug.Log("Going to " + state);

		InTransition = true;
		yield return null;
		SceneManager.LoadScene(sceneid);
		State = state;
		InTransition = false;
	}
}
