﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishing : MonoBehaviour {

    public int numberOfTraitsAcquired;

    private bool heart;

    public GameLogic gameLogic;

    public GameObject fishHook;

    public GameObject HookStart;

    public GameObject HookTarget;

    public GameObject HookedPerson;

    public GameObject fishingRod;

    private Quaternion fishingRodOriginalRotation;

    private Quaternion fishingRodPulledBackRotation;

    public float fishingRodCastSpeed;

    private float fishingRodCasting;


    private Vector3 hookTargetOriginalPosition;

    private Vector3 hookCurrentPos;
    
    private Vector3 HookedPersonLastHookAtBottomPosition;

    

    public float lastHookPersonToPlayerLerpSpeed;

    public float tapHookedPersonToPlayerLength;

    public float hookCurveStrength;
    
    public float maxHoldTime;

    public float playFieldLength;

    private float playerCurrentThrowLength;

    public float throwChargeSpeed;

    public float minimumThrowLength;

    public float hookLooseMaxTime;

    private float hookingPersonLerp;

    private float hookThrowIncrementor;

    private float buttonHoldTimer;

    private float reelTimer;
    

    private bool buttonReleaseEarly;

    private bool ButtonHold;


    public int playerIndex;


    public enum PlayerStates {


        _0_ReadyToThrow,
        _1_ChargingThrow,
        _2_CastingRod,
        _3_Throwing,
        _4_GotHook,
        _5_CanReelIn,
        _6_Reeling,
        _7_ReeledInPerson,
        _8_MergingTwoPersons,
        _9_MissedHook
    };

    public PlayerStates playerStates;


    public Transform personRoot;

	public Person personPrefab;

	public Person person;
    
    public AudioClip[] audioClips;

    //--------------------------------------------------------------------------

	LineRenderer line;

	//--------------------------------------------------------------------------

    void Start () {
        
        playerStates = PlayerStates._0_ReadyToThrow;

        fishingRodOriginalRotation = fishingRod.transform.rotation;


        if (playerIndex == 1)
            fishingRodPulledBackRotation = new Quaternion(fishingRodOriginalRotation.x, fishingRodOriginalRotation.y, fishingRodOriginalRotation.z + .20f, fishingRodOriginalRotation.w);

        else if (playerIndex == 2)
            fishingRodPulledBackRotation = new Quaternion(fishingRodOriginalRotation.x, fishingRodOriginalRotation.y, fishingRodOriginalRotation.z - .20f, fishingRodOriginalRotation.w);


        hookTargetOriginalPosition = HookTarget.gameObject.transform.position;
		line = GetComponent<LineRenderer>();
		person = Instantiate(personPrefab, personRoot);
		person.transform.localPosition = Vector3.zero;
		RefreshFace();
	}

	//--------------------------------------------------------------------------

	public void RefreshFace()
	{
		person.Init(GetMyFace(), 1f);
	}

    //--------------------------------------------------------------------------

    FaceDescription GetMyFace()
	{
		if (gameObject.tag == "Player")
		{
			return Game.Instance.Face1;
		}
		else
		{
			return Game.Instance.Face2;
		}
	}

    //--------------------------------------------------------------------------

    void SetMyFace(FaceDescription face)
	{
		if (gameObject.tag == "Player")
		{
			Game.Instance.Face1 = face;
		}
		else
		{
			Game.Instance.Face2 = face;
		}
	}


    //--------------------------------------------------------------------------
    // Checks after each hooked person how many traits the player has, if they have all the traits then the game is over. 

    void NumberOfTraits() {

        if (gameObject.tag == "Player") {

            var list = FaceDescription.CompareTrait(Game.Instance.Face1, Game.Instance.IdealFace);

            foreach (var item in list) {

                Debug.Log("Player Nr of Traits:" + list.Count);

                numberOfTraitsAcquired = list.Count;
            }

            if (numberOfTraitsAcquired == 8)
                gameLogic.player1Won = true;

        }

        else {

            var list2 = FaceDescription.CompareTrait(Game.Instance.Face2, Game.Instance.IdealFace);

            foreach (var item in list2) {

                Debug.Log("Player 2 Nr of Traits:" + list2.Count);
                numberOfTraitsAcquired = list2.Count;
            }

            if (numberOfTraitsAcquired == 8) 
                gameLogic.player2Won = true;

        }





    }

    //--------------------------------------------------------------------------

    private void LateUpdate()
	{
		line.SetPosition(0, HookStart.transform.position);
		line.SetPosition(1, fishHook.transform.position);
	}

    //--------------------------------------------------------------------------

    void FixedUpdate () {

        // Player 1 Input
        if (gameObject.tag == "Player") {

            // Player 1 Button Down
            if (gameLogic.GetButtonDown(0))
                ButtonDown();

            // Player 1 Button Up
            if (gameLogic.GetButtonUp(0)) 
                ButtonUp();
        }

        // Player 2 Input
        if (gameObject.tag == "Player2") {

            // Player 2 Button Down
            if (gameLogic.GetButtonDown(1))
                ButtonDown();

            // Player 2 Button Up
            if (gameLogic.GetButtonUp(1))
                ButtonUp();
        }
	
	//--------------------------------------------------------------------------

        // Button Hold when ready to throw so it starts charging
        if (ButtonHold && playerStates == PlayerStates._1_ChargingThrow)
            PlayerChargingThrow();

        // If Casting Rod Forward then rotates it back to original position
        if (playerStates == PlayerStates._2_CastingRod)
            CastingRod();

        // Throwing Fish Rod
        if (playerStates == PlayerStates._3_Throwing)
            FishHookCurveMovement();

        // Reel Lost Timer
        if (playerStates == PlayerStates._5_CanReelIn || playerStates == PlayerStates._6_Reeling) {

            reelTimer += Time.deltaTime;

            if (reelTimer > hookLooseMaxTime)
                HookLost();
        }

        // Reeling in Person
        if (playerStates == PlayerStates._7_ReeledInPerson)
            HookedUpPersonToPlayer();

        // If Released Throw Button Early then keeps button hold so it throws at least 1 Meter. 
        if (buttonReleaseEarly)
            ButtonUp();
    }

    //--------------------------------------------------------------------------

    void HookLost() {

        HookedPerson.GetComponent<AIMovement>().PlayerLostHook(playerIndex);

        ResetFishing();
    }

    //--------------------------------------------------------------------------

    public void ButtonDown() {

        if (playerStates == PlayerStates._0_ReadyToThrow) {

            // Start Arm Rod Animation

            HookTarget.SetActive(true);

            ButtonHold = true;
            buttonHoldTimer = 0;

            playerStates = PlayerStates._1_ChargingThrow;
        }

        // If player have hook and they can have played their hook animation and can be reeled in. 
        else if (playerStates == PlayerStates._5_CanReelIn || playerStates == PlayerStates._6_Reeling) {

            reelTimer = 0; // Resets reelTimer when tapping
            
            if (playerIndex == 1) {

                // Hooks up them to the tree
                if (HookedPerson.gameObject.transform.position.x < -HookedPerson.GetComponent<AIMovement>().lengthToGetReeledIn) {

                    playerStates = PlayerStates._7_ReeledInPerson;

                    // Calls HookedUpByPlayer on the person, it checks which players had hooked on it and calls Reset on the other one.
                    HookedPerson.GetComponent<AIMovement>().HookedUpByPlayer(playerIndex);

                    HookedPerson.GetComponent<People>().PlayHookedUpSound();
                    HookedPersonLastHookAtBottomPosition = HookedPerson.gameObject.transform.position;
                }

                // Reels them closer to the tree. 
                else {

                    playerStates = PlayerStates._6_Reeling;
                    HookedPerson.gameObject.transform.position = new Vector3((HookedPerson.gameObject.transform.position.x + tapHookedPersonToPlayerLength), HookedPerson.gameObject.transform.position.y, HookedPerson.gameObject.transform.position.z);

                    HookedPerson.GetComponent<AIMovement>().currentPosition = HookedPerson.gameObject.transform.position;

                    GetComponent<AudioSource>().PlayOneShot(audioClips[1]);
                }
            }

            else if (playerIndex == 2) {

                // Hooks up them to the tree
                if (HookedPerson.gameObject.transform.position.x > HookedPerson.GetComponent<AIMovement>().lengthToGetReeledIn) {

                    playerStates = PlayerStates._7_ReeledInPerson;

                    // Calls HookedUpByPlayer on the person, it checks which players had hooked on it and calls Reset on the other one.
                    HookedPerson.GetComponent<AIMovement>().HookedUpByPlayer(playerIndex);

                    HookedPerson.GetComponent<People>().PlayHookedUpSound();
                    HookedPersonLastHookAtBottomPosition = HookedPerson.gameObject.transform.position;
                }

                // Reels them closer to the tree. 
                else {

                    playerStates = PlayerStates._6_Reeling;
                    HookedPerson.gameObject.transform.position = new Vector3((HookedPerson.gameObject.transform.position.x + tapHookedPersonToPlayerLength), HookedPerson.gameObject.transform.position.y, HookedPerson.gameObject.transform.position.z);

                    HookedPerson.GetComponent<AIMovement>().currentPosition = HookedPerson.gameObject.transform.position;

                    GetComponent<AudioSource>().PlayOneShot(audioClips[1]);
                }
            }
        }
    }

    //--------------------------------------------------------------------------

    public void ButtonUp() {

        // If Holding button more than 1 Meter, 
        if (playerStates == PlayerStates._1_ChargingThrow && playerCurrentThrowLength > minimumThrowLength) {

            ButtonHold = false;

            playerStates = PlayerStates._2_CastingRod;
        }

        // If release Button Early it continous to Hold it down to 1 Meter length. 
        else if (playerStates == PlayerStates._1_ChargingThrow && playerCurrentThrowLength < minimumThrowLength)
            buttonReleaseEarly = true;
    }

    //--------------------------------------------------------------------------
    // Casting Rod Forward

    void CastingRod() {

        if (fishingRod.transform.rotation != fishingRodOriginalRotation) {

            fishingRod.transform.rotation = Quaternion.Lerp(fishingRodPulledBackRotation, fishingRodOriginalRotation, (fishingRodCasting += fishingRodCastSpeed) * Time.deltaTime);
        }

        else
            StartCoroutine(ThrowRod(0));

    }

    //--------------------------------------------------------------------------
    // Delay for when the arm rotates back and throws the Rod, after delay sets state to Throwing. 

    IEnumerator ThrowRod(float delay) {

        yield return new WaitForSeconds(delay);

        playerStates = PlayerStates._3_Throwing;

        GetComponent<AudioSource>().PlayOneShot(audioClips[0]);
    }

    //--------------------------------------------------------------------------
    // Player One Hold, Sets Hold Timer, Throw Range and Fish Hook Position

    void PlayerChargingThrow() {

        //--------------------------------------------------------------------------
        // Sets Button Hold Timer and Anim Phases depending on it. 

        if (buttonHoldTimer < maxHoldTime) {
            buttonHoldTimer += Time.deltaTime;
            
            fishingRod.transform.rotation = Quaternion.Lerp(fishingRodOriginalRotation, fishingRodPulledBackRotation, playerCurrentThrowLength / playFieldLength);
        }
        else
            buttonHoldTimer = 3;

        //--------------------------------------------------------------------------

        // Throw Range, checks if more than maximum
        if (playerCurrentThrowLength < playFieldLength)
            playerCurrentThrowLength += (playFieldLength / maxHoldTime) / throwChargeSpeed;
        else
            playerCurrentThrowLength = playFieldLength;

        // Sets Fish Hook Child Component
        if (gameObject.tag == "Player")
            HookTarget.gameObject.transform.localPosition = new Vector3 (playerCurrentThrowLength, HookTarget.gameObject.transform.localPosition.y, HookTarget.gameObject.transform.localPosition.z);

        else if (gameObject.tag == "Player2")
            HookTarget.gameObject.transform.localPosition = new Vector3(-playerCurrentThrowLength, HookTarget.gameObject.transform.localPosition.y, HookTarget.gameObject.transform.localPosition.z);
    }

    //--------------------------------------------------------------------------

    void FishHookCurveMovement() {
        
        // Speed of Throw
        hookThrowIncrementor += 0.025f;

        // Lerp the Hook
        hookCurrentPos = Vector3.Lerp(HookStart.gameObject.transform.localPosition, HookTarget.gameObject.transform.localPosition, hookThrowIncrementor);

        // Setting a Hook Height so it gets curve
        hookCurrentPos.y += hookCurveStrength * Mathf.Sin(Mathf.Clamp01(hookThrowIncrementor) * Mathf.PI);

        // Setting FishHook position
        fishHook.gameObject.transform.localPosition = hookCurrentPos;

        // When Reaching the Target. If reached with no Hit then it was miss and resets everything. 
        if (fishHook.gameObject.transform.localPosition == HookTarget.gameObject.transform.localPosition) {
            ResetFishing();
            HookTarget.SetActive(false);
        }
            
    }

    //--------------------------------------------------------------------------
    // When Person is Lerping to player. 

    void HookedUpPersonToPlayer() {

        if (!heart) {

            GetComponent<Animation>().Play();
            heart = true;
        }

        // Lerps the person to player if they are not at the same position
        if (HookedPerson.gameObject.transform.position != gameObject.transform.position)
            HookedPerson.gameObject.transform.position = Vector3.Lerp(HookedPersonLastHookAtBottomPosition, gameObject.transform.position, (hookingPersonLerp += Time.deltaTime) * lastHookPersonToPlayerLerpSpeed);

        // When they are at same position, sets states and calls Merge Sequence etc. 
        else {

            heart = false;
            
            HookedPerson.gameObject.GetComponent<SphereCollider>().enabled = false;

            playerStates = PlayerStates._8_MergingTwoPersons;
            
            StartCoroutine(MergeSequence());
        }
    }

    //--------------------------------------------------------------------------
    // Merges the Faces and calls Reset Fishing so the player can Throw again. 

    IEnumerator MergeSequence() {

		//Do heart effect
		yield return null;

		//wait for hide

		var myFace = GetMyFace();
		var newFace = FaceDescription.Combine(myFace, HookedPerson.GetComponent<People>().Desc, Game.Instance.IdealFace);
		SetMyFace(newFace);
		RefreshFace();

		Destroy(HookedPerson.gameObject);
		//perform switch

		//remove effect
        
		ResetFishing();
	}

    //--------------------------------------------------------------------------
    // Resets Fishing and all the variables and states it uses. 

    public void ResetFishing() {

        NumberOfTraits();
        
        playerStates = PlayerStates._0_ReadyToThrow;

        reelTimer = 0;
        buttonHoldTimer = 0;
        buttonReleaseEarly = false;
        playerCurrentThrowLength = 0;

        fishHook.GetComponent<SphereCollider>().enabled = true;
        fishHook.gameObject.transform.localPosition = HookStart.gameObject.transform.localPosition;

        HookedPerson = null;
        HookTarget.gameObject.transform.position = hookTargetOriginalPosition;
        hookingPersonLerp = 0;
        hookCurrentPos = new Vector3(0, 0, 0);
        hookThrowIncrementor = 0;
    }

    //--------------------------------------------------------------------------
    // When Reeling in, de activated Fish Hook Collider so it doesnt collide while again while on Person. 

    public void DeActivateFishHookOnReel() {

        fishHook.GetComponent<SphereCollider>().enabled = false;
    }
}
