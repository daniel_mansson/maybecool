﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Menu : MonoBehaviour {
    public GameObject tutorialPopup;

    public Button playButton;

    public AudioSource buttonSound;
    public AudioClip audioClip;

	// Use this for initialization
	void Start () {
        tutorialPopup.SetActive(false);
        buttonSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

    // start the game when clicked on Play button
    void Play ()
    { 
        buttonSound.PlayOneShot(buttonSound.clip);
        Game.Instance.GoToGame();
    }
    // open the tutorial popup when clicked on the Tutorial button
    public void TutorialClicked() {
        buttonSound.PlayOneShot(buttonSound.clip);
        tutorialPopup.SetActive(true);
    }

    public void TutorialDisabled() {
        tutorialPopup.SetActive(false);
    }

    public void PlayClip()
    {
        buttonSound.clip = audioClip;
        buttonSound.Play();
    }
        
}
