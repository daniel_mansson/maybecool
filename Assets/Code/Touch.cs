﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch : MonoBehaviour {

    public GameLogic gameLogic;

    public GameObject player;

    public GameObject player2;

    // Update is called once per frame
    void Update () {


        for (var i = 0; i < Input.touchCount; ++i) {

            if (Input.GetTouch(i).phase == TouchPhase.Began) {

                RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position), Vector2.zero);
                // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this

                if (hitInfo) {

                    if (hitInfo.transform.gameObject.tag == "P1Touch") {

                        player.GetComponent<Fishing>().ButtonDown();
                    }

                    else if (hitInfo.transform.gameObject.tag == "P2Touch") {

                        player2.GetComponent<Fishing>().ButtonDown();
                    }
                    
                    // Here you can check hitInfo to see which collider has been hit, and act appropriately.
                }
            }
        }


        for (var i = 0; i < Input.touchCount; ++i) {

            if (Input.GetTouch(i).phase == TouchPhase.Ended) {

                RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position), Vector2.zero);
                // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this

                if (hitInfo) {

                    if (hitInfo.transform.gameObject.tag == "P1Touch") {

                        player.GetComponent<Fishing>().ButtonUp();
                    }

                    else if (hitInfo.transform.gameObject.tag == "P2Touch") {

                        player2.GetComponent<Fishing>().ButtonUp();
                    }

                    // Here you can check hitInfo to see which collider has been hit, and act appropriately.
                }
            }
        }



        /*
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Clicked");
            Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
            // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
            if (hitInfo)
            {
                Debug.Log(hitInfo.transform.gameObject.name);
                // Here you can check hitInfo to see which collider has been hit, and act appropriately.
            }
        }
        */








        /*
        Vector2 vTouchPos = Input.GetTouch(0).position;

        Ray ray = Camera.main.ScreenPointToRay(vTouchPos);

        RaycastHit castHit;

        if (Physics.Raycast(ray.origin, ray.direction, out castHit))
        {
            if (castHit.transform.tag == "P1Touch")
                Debug.Log("Test");
        }
        */
    }
}
