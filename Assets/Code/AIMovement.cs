﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour {
    
    public Vector3 currentPosition;

    private Vector3 offScreenStartPosition;

    private Vector3 targetPosition;


    public float outsideScreenPositionY;

    public float firstInsideScreenPositionY;

    public float lengthToGetReeledIn;

    public float moveLengthOnX;

    public float maxMoveLengthTop;

    public float maxMoveLengthBottom;

    public float movementLengthX;

    public float movementLengthY;

    public float minimumMovementSpeed;

    public float maximumMovementSpeed;

    public float movementDelayMin;

    public float movementDelayMax;

    public float timeAllowedOnScreen;

    private float currentMovementSpeed;

    private float movementLerp;
    
    private float timer;



    private bool moveToTarget;

    private bool moveToFirstTarget;

    private bool destroySelf;

    private bool canGetHooked;

    private bool isHooked;

    private bool hookedByP1;

    private bool hookedByP2;


    public GameObject playerOne;

    public GameObject playerTwo;


    //-------------------------------------------------------------------
    // Start sets movement speed with randoms, sets random offscreen position and currentPosition. 

    void Start () {

        playerOne = GameObject.FindGameObjectWithTag("Player");

        playerTwo = GameObject.FindGameObjectWithTag("Player2");

        canGetHooked = true;

        currentMovementSpeed = Random.Range(minimumMovementSpeed, maximumMovementSpeed + 1);

        offScreenStartPosition.x = Random.Range(-moveLengthOnX, moveLengthOnX); ;
        offScreenStartPosition.y = outsideScreenPositionY;

        currentPosition = offScreenStartPosition;

        transform.position = offScreenStartPosition;

        targetPosition = offScreenStartPosition;
        targetPosition.y = firstInsideScreenPositionY;

        moveToTarget = true;
        moveToFirstTarget = true;
    }

    //-------------------------------------------------------------------
    // Update adds to timer and calls moveToTarget if the bool is true

    void FixedUpdate () {

        if (!isHooked) {
            timer += Time.deltaTime;

            if (moveToTarget)
                MoveToTarget();
        }

        else if (isHooked) {

            if (hookedByP1)
                playerOne.GetComponent<Fishing>().fishHook.gameObject.transform.position = gameObject.transform.position;

            if (hookedByP2)
                playerTwo.GetComponent<Fishing>().fishHook.gameObject.transform.position = gameObject.transform.position;
        }
    }

    //-------------------------------------------------------------------
    // When player lost hook, checks if opposing player is still hooked, if not call Flee Scene method. 

    public void PlayerLostHook(int playerID) {

        if (playerID == 1) {

            hookedByP1 = false;

            if (!hookedByP2) {

                FleeAfterHookLost();
            }
        }

        else if (playerID == 2) {

            hookedByP2 = false;

            if (!hookedByP1) {

                FleeAfterHookLost();
            }
        }
    }

    //-------------------------------------------------------------------
    // After player lost hook to person and its hooked to no one it flees the scene. 

    void FleeAfterHookLost() {

        isHooked = false;
        canGetHooked = false;
        movementLerp = 0;
        moveToTarget = true;
        targetPosition = currentPosition;
        targetPosition.y = outsideScreenPositionY;
        destroySelf = true;
        currentMovementSpeed = 1.5f;
    }

    //-------------------------------------------------------------------
    // When Person gets Hooked up by player into tree. Checks who hooked him up and if he was hooked by the other player as well. 

    public void HookedUpByPlayer(int playerHookedUpBy) {

        if (playerHookedUpBy == 1 && hookedByP2 == true) {

            hookedByP2 = false;
            playerTwo.GetComponent<Fishing>().ResetFishing();
        }


        else if (playerHookedUpBy == 2 && hookedByP1 == true) {

            hookedByP1 = false;
            playerOne.GetComponent<Fishing>().ResetFishing();
        }
    }

    //-------------------------------------------------------------------
    // Lerps the gameobject to target position. Checks if should destroy self otherwise calls AtTarget();

    void MoveToTarget() {

        // Moving To Target
        if (transform.position != targetPosition) {
            
            transform.position = Vector2.Lerp(currentPosition, targetPosition, (movementLerp += Time.deltaTime) * currentMovementSpeed);
        }

        // When At Target
        else {

            if (destroySelf) // When at target and is set to destroy self then doest that
                Destroy(gameObject);

            currentPosition = transform.position;
            moveToTarget = false;
            AtTarget(); // Calls AtTarget when arrived at taret
        }
    }

    //-------------------------------------------------------------------
    // Calls when actor is on target, checks timer and calls Coroutine Delay

    void AtTarget() {

        // Checks if Person has been on screen more than allowed, if so calls coroutine but with leave to true. 
        if (timer > timeAllowedOnScreen) 
            StartCoroutine(MovementDelay(Random.Range(movementDelayMin, movementDelayMax), true));

        else
            StartCoroutine(MovementDelay(Random.Range(movementDelayMin, movementDelayMax), false));

        // If its the initial move to bottom of screen then cant be hooked until at target. 
        if (moveToFirstTarget)
            moveToFirstTarget = false;
    }

    //-------------------------------------------------------------------
    // Gets New Target Position, checks wether the gameobject is near the edges, if so forces it to move the oppososite way. Sets the target with random from maximum variables

    Vector3 GetNewTargetPosition() {

        Vector3 newTargetLocation = transform.position;

        bool cantMoveLeft = false;
        bool cantMoveRight = false;
        bool cantMoveDown = false;
        bool cantMoveUp = false;

        //-------------------------------------------------------------------

        // If too close to Left
        if (transform.position.x < -moveLengthOnX + 1)
            cantMoveLeft = true;

        // If too close to Right
        else if (transform.position.x > moveLengthOnX - 1)
            cantMoveRight = true;

        // If too close to Bottom
        if (transform.position.y < maxMoveLengthBottom + 1)
            cantMoveDown = true;

        // If too over Top
        else if (transform.position.y > maxMoveLengthTop - 1)
            cantMoveUp = true;

        //-------------------------------------------------------------------
        
        // If neither to close to left or right then can Randomise move on X
        if (!cantMoveLeft && !cantMoveRight)
            newTargetLocation.x += Random.Range(-movementLengthX, movementLengthX);

        // If to close to Left, forces it to move right
        else if (cantMoveLeft)
            newTargetLocation.x += movementLengthX;

        // If to close to Right, forces it to move left
        else if (cantMoveRight)
            newTargetLocation.x += -movementLengthX;
        
        //-------------------------------------------------------------------
  
        // If neither to close to bottom or top then can Randomise move on Y
        if (!cantMoveUp && !cantMoveDown)
            newTargetLocation.y += Random.Range(-movementLengthY, movementLengthY);

        // If to close to top, force it to go down
        else if (cantMoveUp)
            newTargetLocation.y += -movementLengthY;

        // if to close to bottom, forces it to go up
        else if (cantMoveDown)
            newTargetLocation.y += movementLengthY;

        //-------------------------------------------------------------------

        return newTargetLocation;
    }

    //-------------------------------------------------------------------
    // Delay that gets called from AtTarget(), gets new target location after the delay. 
    // if set to leave then sets the new target position to be outside the screen and then sets bool to destroy self so when it arrives it destroyes itself.

    IEnumerator MovementDelay(float Delay, bool leave) {

        yield return new WaitForSeconds(Delay);

        // If not set to leave and it is not hooked, gets new a position
        if (!leave && !isHooked)
            targetPosition = GetNewTargetPosition();

        // If set to leave and is not hooked, sets targetPosition to be outSide screen and destroySelf to true so when it arrives there it destroys self. 
        else if (leave && !isHooked) {
            targetPosition = currentPosition;
            targetPosition.y = outsideScreenPositionY;

            destroySelf = true;
        }
        
        if (!isHooked) {
            movementLerp = 0; // Resets it so it kan lerp again. 
            moveToTarget = true; // Sets move to Target to true so it is called in update
        }
    }

    //-------------------------------------------------------------------
    // OnTriggerEnter called when get hit by FishHook

    void OnTriggerEnter (Collider other) {

        if (canGetHooked) {

            if (moveToFirstTarget)
                moveToFirstTarget = false;

            // If hooked by Player 1
            if (other.tag == "P1Hook") {

                canGetHooked = false; // false so it cant be hooked by anyone else until potential hook animation plays out

                moveToTarget = false;

                hookedByP1 = true;

                playerOne.GetComponent<Fishing>().playerStates = Fishing.PlayerStates._4_GotHook; // Sets player one state

                playerOne.GetComponent<Fishing>().HookTarget.SetActive(false);

                playerOne.GetComponent<Fishing>().HookedPerson = gameObject; // Sets its varaible to this gameObject

                playerOne.GetComponent<Fishing>().fishHook.gameObject.transform.position = gameObject.transform.position; // Sets Fishhook position
                playerOne.GetComponent<Fishing>().DeActivateFishHookOnReel(); // De-Activates Fishhook collider

                StartCoroutine( Hooked () );
            }

            // If hooked by Player 2
            else if (other.tag == "P2Hook") {

                canGetHooked = false;

                moveToTarget = false;

                hookedByP2 = true;

                playerTwo.GetComponent<Fishing>().playerStates = Fishing.PlayerStates._4_GotHook;

                playerTwo.GetComponent<Fishing>().HookTarget.SetActive(false);

                playerTwo.GetComponent<Fishing>().HookedPerson = gameObject;

                playerTwo.GetComponent<Fishing>().fishHook.gameObject.transform.position = gameObject.transform.position;
                playerTwo.GetComponent<Fishing>().DeActivateFishHookOnReel();

                StartCoroutine( Hooked() );
            }
        }
    }

    //-------------------------------------------------------------------
    // When hooked, checks if it is already hooked, if not then plays animation, after animation sets bools and states to players so they can reel in. 

    IEnumerator Hooked() {

        if (!isHooked) {

            // Play Hooked Animation
            
            isHooked = true;

            yield return new WaitForSeconds(0);

            canGetHooked = true; // After animation and being dragged away it can get hooked again. 

            // Sets State depending on which player that hooked. 
            if (hookedByP1) 
                playerOne.GetComponent<Fishing>().playerStates = Fishing.PlayerStates._5_CanReelIn;

            else if (hookedByP2) 
                playerTwo.GetComponent<Fishing>().playerStates = Fishing.PlayerStates._5_CanReelIn;
        }

        // If alreday hooked by either player.
        else {

            // Play some hit sound and maybe some stars sprite to give feedback that it got hit again.  

            canGetHooked = true;

            if (hookedByP1) 
                playerTwo.GetComponent<Fishing>().playerStates = Fishing.PlayerStates._5_CanReelIn;

            if (hookedByP2) 
                playerOne.GetComponent<Fishing>().playerStates = Fishing.PlayerStates._5_CanReelIn;
        }
        
        yield return new WaitForSeconds(0);
    }
}

