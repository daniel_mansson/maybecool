﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;





public enum TraitType
{
	Head,
	Hair,
	Eyes,
	Eyebrows,
	Nose,
	Mouth,
	Ears,
	Freckles
}

[Serializable]
public class Trait
{
	public TraitType type;
	public int id;
}

[Serializable]
public class FaceDescription
{
	public List<Trait> traits = new List<Trait>();

	public static int Compare(FaceDescription a, FaceDescription b)
	{
		return a.traits.Sum(t => b.traits.Count(t2 => t2.type == t.type && t2.id == t.id));
	}

    public static List<Trait> CompareTrait(FaceDescription a, FaceDescription b)
    {
        var list = new List<Trait>();

        foreach (var t in a.traits)
        {
            foreach(var t2 in b.traits){
                if(t.type == t2.type && t.id == t2.id){
                    list.Add(t);
                }
            }
        }

        return list;
    }

	public static FaceDescription Combine(FaceDescription a, FaceDescription b, FaceDescription target)
	{
		var result = new FaceDescription();

		foreach (var t in a.traits)
		{
			var sameIdeal = target.traits.First(i => i.type == t.type);

			if (t.id == sameIdeal.id)
			{
				result.traits.Add(t);
			}
			else
			{
				var other = b.traits.FirstOrDefault(t2 => t2.type == t.type);

				if (UnityEngine.Random.Range(0, 2) == 0 || other.id == sameIdeal.id)
				{
					result.traits.Add(other);
				}
				else
				{
					result.traits.Add(t);
				}
			}
		}

		return result;
	}

	public static FaceDescription GetRandomExcept(FaceDescription desc)
	{
		var r = new FaceDescription();

		foreach (var item in System.Enum.GetValues(typeof(TraitType)))
		{
			var t = (TraitType)item;

			var d = desc.traits.First(aa => aa.type == t);
			while (true)
			{
				int id = UnityEngine.Random.Range(1, 5);
				if (d.id != id)
				{
					r.traits.Add(new Trait()
					{
						id = id,
						type = t
					});
					break;
				}
			}
		}

		return r;
	}

	public static FaceDescription GetRandom()
	{
		return new FaceDescription()
		{
			traits = new List<Trait>()
			{
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Ears
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Eyebrows
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Eyes
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Freckles
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Hair
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Head
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Mouth
				},
				new Trait()
				{
					id = UnityEngine.Random.Range(1, 5),
					type = TraitType.Nose
				}
			}
		};
	}
}

public class Face : MonoBehaviour {

    private GameObject GameLogic;
    
    [Serializable]
	public class FaceSprite
	{
		public TraitType type;
		public SpriteRenderer renderer;
	}

	List<FaceSprite> sprites = new List<FaceSprite>();

	private void Awake()
	{

        GameLogic = GameObject.FindGameObjectWithTag("GameLogic");

		foreach (var renderer in GetComponentsInChildren<SpriteRenderer>(includeInactive: true))
		{
			foreach (var name in Enum.GetNames(typeof(TraitType)))
			{
				if (renderer.name.Contains(name.ToLower())) {

					sprites.Add(new FaceSprite() {
                        
						renderer = renderer, type = (TraitType)Enum.Parse(typeof(TraitType), name)
					});

                    // For each face that gets spawned takes the layer and adds spriteOrder
                    if (GameLogic && !GameLogic.GetComponent<GameLogic>().gameOver)
                        renderer.sortingOrder += GameLogic.GetComponent<GameLogic>().spriteOrder;

                }
            }
		}

        // After each face it adds 4 to sprite order so the next face will get its layers added so they will get unique layer. 
        if (GameLogic && !GameLogic.GetComponent<GameLogic>().gameOver)
            GameLogic.GetComponent<GameLogic>().spriteOrder += 4;
    }

	public FaceDescription faceDesc;

	public void Init(FaceDescription data)
	{
		faceDesc = data;

		foreach (var trait in data.traits)
		{
			var faceSprite = sprites.FirstOrDefault(s => s.type == trait.type);

			string resourceName = trait.type.ToString().ToLower() + trait.id;
			//Debug.Log(" " + resourceName);
			faceSprite.renderer.sprite = Resources.Load<Sprite>(resourceName);
		}
	}

	[ContextMenu("Shuffle")]
	public void Shuffle()
	{
		var d = FaceDescription.GetRandom();

		Init(d);
	}
}
