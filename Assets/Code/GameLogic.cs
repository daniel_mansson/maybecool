﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{


    public int spriteOrder;
    public bool player1Won;
    public bool player2Won;



    public static GameLogic CurrentLogic { get; private set; }

	[SerializeField]
	Camera mainCamera;

	[Header("Start sequence")]
	public bool quickStart = false;
	[SerializeField]
	Animation cloudEnterAnimation;
	[SerializeField]
	AnimationCurve startCameraInCurve;
	[SerializeField]
	AnimationCurve startCameraOutCurve;
	[SerializeField]
	float cameraMoveTime = 1f;
	[SerializeField]
	float cameraWaitTime = 1f;
	[SerializeField]
	Camera zoomInCamera;
	[SerializeField]
	Text bigText;
	[SerializeField]
	Face idealFace;

	[Header("Ingame")]
	[SerializeField]
	People peoplePrefab;
	[SerializeField]
	float spawnFrequencyMin;
	[SerializeField]
	float spawnFrequencyMax;
	[SerializeField]
	AnimationCurve spawnFrequencyCurve;
	[SerializeField]
	Transform spawnPos1;
	[SerializeField]
	Transform spawnPos2;
	[SerializeField]
	float gameDuration = 60f;
	[SerializeField]
	Countdown countdown;

	float gameTimer = 0f;
	float spawnTimer = 0f;

	public bool gameOver = false;
	bool inputEnabled = false;
	Vector3 cameraStartPos;
	float cameraStartSize;

	bool input1 = false;
	bool inputprev1 = false;

	bool input2 = false;
	bool inputprev2 = false;

	public bool GetButton(int playerId)
	{
		if (playerId < 0 || playerId > 1)
			return false;

		return playerId == 0 ? input1 : input2;
	}

	public bool GetButtonDown(int playerId)
	{
		if (playerId < 0 || playerId > 1)
			return false;

		if (playerId == 0)
		{
			return input1 && !inputprev1;
		}
		else
		{
			return input2 && !inputprev2;
		}
	}

	public bool GetButtonUp(int playerId)
	{
		if (playerId < 0 || playerId > 1)
			return false;

		if (playerId == 0)
		{
			return !input1 && inputprev1;
		}
		else
		{
			return !input2 && inputprev2;
		}
	}

	private void Awake()
	{
		CurrentLogic = this;
	}

	void Start()
	{
		gameTimer = gameDuration;
		//inputEnabled = true;

		cameraStartPos = mainCamera.transform.position;
		cameraStartSize = mainCamera.orthographicSize;
		StartCoroutine(GameSequence());
	}

	private void OnDestroy()
	{
		CurrentLogic = null;
	}

	void Update()
	{
		inputprev1 = input1;
		inputprev2 = input2;

		if (inputEnabled)
		{
			input1 = Input.GetKey(KeyCode.A);
			input2 = Input.GetKey(KeyCode.K);
		}
		else
		{
			input1 = input2 = false;
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Game.Instance.GoToMenu();
		}
	}

	IEnumerator GameSequence()
	{
		countdown.gameObject.SetActive(false);

		Debug.Log("Playing start sequence");
		if (quickStart)
		{
			//Enter game sequence
			idealFace.Init(Game.Instance.IdealFace);
			idealFace.transform.localScale = Vector3.one;
			cloudEnterAnimation.Play();
		}
		else
		{
			yield return StartCoroutine(StartGameSequence());
		}

		//Start playing
		inputEnabled = true;

		Debug.Log("Game starting!");
		countdown.gameObject.SetActive(true);

		yield return StartCoroutine(PerformGameLogic());

        if (!player1Won && !player2Won)
        {
            countdown.gameObject.SetActive(false);
            //Stop playing
            inputEnabled = false;

            Debug.Log("Game ended!");

            //End of game sequence
            yield return StartCoroutine(EndGameSequence());

            Debug.Log("Game over! Back to menu!");
            Game.Instance.GoToGameOver();
        }

        else
        {
            countdown.gameObject.SetActive(false);
            //Stop playing
            inputEnabled = false;

            Debug.Log("Game ended!");

            //End of game sequence
            yield return StartCoroutine(EndGamePlayerWasFirst());

            Debug.Log("Game over! Back to menu!");
            Game.Instance.GoToGameOver();
        }
	}

	IEnumerator StartGameSequence()
	{
		yield return new WaitForSecondsRealtime(1);

		cloudEnterAnimation.Play();

		yield return new WaitForSeconds(0.8f);

		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / cameraMoveTime;
			mainCamera.transform.position = Vector3.Lerp(cameraStartPos, zoomInCamera.transform.position, startCameraInCurve.Evaluate(t));
			mainCamera.orthographicSize = Mathf.Lerp(cameraStartSize, zoomInCamera.orthographicSize, startCameraInCurve.Evaluate(t));
			yield return null;
		}

		while (cloudEnterAnimation.isPlaying)
			yield return null;

		yield return new WaitForSeconds(0.5f);

		bigText.text = "\n\nTHE IDEAL";

		idealFace.Init(Game.Instance.IdealFace);
		//idealFace.gameObject.SetActive(true);

		t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / 0.5f;
			idealFace.transform.localScale = Vector3.one * Mathf.Clamp01(t);
			yield return null;
		}

		yield return new WaitForSeconds(cameraWaitTime);

		bigText.text = "";

		t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / cameraMoveTime;
			mainCamera.transform.position = Vector3.Lerp(zoomInCamera.transform.position, cameraStartPos, startCameraOutCurve.Evaluate(t));
			mainCamera.orthographicSize = Mathf.Lerp(zoomInCamera.orthographicSize, cameraStartSize, startCameraOutCurve.Evaluate(t));
			yield return null;
		}

		bigText.text = "GET\nREADY!";
		yield return new WaitForSeconds(1.2f);
		bigText.text = "3!";
		yield return new WaitForSeconds(0.6f);
		bigText.text = "2!";
		yield return new WaitForSeconds(0.6f);
		bigText.text = "1!";
		yield return new WaitForSeconds(0.6f);
		bigText.text = "GO!";

		StartCoroutine(RemoveGO());
	}

	IEnumerator RemoveGO()
	{
		yield return new WaitForSeconds(1f);
		bigText.text = "";
	}

	IEnumerator EndGameSequence()
	{
		bigText.text = "TIME'S UP";
		yield return new WaitForSeconds(2);
	}

    IEnumerator EndGamePlayerWasFirst()
    {
        if (player1Won)
            bigText.text = "Player 1 Won";

        else if (player2Won)
            bigText.text = "Player 2 Won";

        yield return new WaitForSeconds(2);
    }

    IEnumerator PerformGameLogic()
	{
		while (!gameOver && !player1Won && !player2Won)
		{
			spawnTimer -= Time.deltaTime * spawnFrequencyCurve.Evaluate(1 - gameTimer / gameDuration);

			if (spawnTimer < 0f)
			{
				spawnTimer = Random.Range(spawnFrequencyMin, spawnFrequencyMax);
				var p = Instantiate(peoplePrefab, Vector3.Lerp(spawnPos1.position, spawnPos2.position, Random.value), Quaternion.identity);

                //p.GetComponent<>

				//TODO init p and skew traits towards ideal
			}

			yield return null;

			gameTimer -= Time.deltaTime;
			countdown.SetTime((int)gameTimer);

			if (gameTimer <= 0)
				gameOver = true;
		}

	}
}
