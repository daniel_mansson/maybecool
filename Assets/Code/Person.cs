﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
	public Face face;
	public GameObject root;
	public GameObject head;
	public GameObject body;

	public float damping;
	public float power;
	public float powerFreq;
	public float maxAmpl;
	public float maxFreq;

	public float headMax;
	public float headDamp;
	public float headForce;
	public float headForcePush;

	Vector3 prevPos;
	float accumVel;

	float velX;
	float posX;

	float ampl;
	float freq;
	float timer;

	public void Init(FaceDescription desc, float size)
	{
		face.Init(desc);
		root.transform.localScale = Vector3.one * size;
	}

	void Start ()
	{
		prevPos = transform.position;
	}
	
	void Update ()
	{
		var pos = transform.position;

		var dx = pos.x - prevPos.x;
		velX += dx * Time.deltaTime * headForcePush;
		velX -= posX * Time.deltaTime * headForce;
		velX *= Mathf.Clamp01(1f - Time.deltaTime * headDamp);

		posX += velX * Time.deltaTime;
		posX = Mathf.Clamp(posX, -headMax, headMax);

		if (Mathf.Abs(posX) > headMax - 0.1f)
		{
			velX *= Mathf.Clamp01(1f - Time.deltaTime * headDamp * 8f);
		}

		head.transform.localRotation = Quaternion.Euler(0, 0, posX);

		var vel = Vector3.Distance(pos, prevPos);
		accumVel += vel;
		accumVel *= Mathf.Clamp01(1f - Time.deltaTime * damping);

		prevPos = pos;

	//	Debug.Log(accumVel);
		freq = Mathf.Clamp(accumVel * powerFreq, 0, maxFreq);
		ampl = Mathf.Clamp(accumVel * power, 0, maxAmpl);

		timer += Time.deltaTime * freq;

		body.transform.localRotation = Quaternion.Euler(0,0, Mathf.Cos(timer) * ampl);
	}
}
