﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExamplePlayerInput : MonoBehaviour
{
	public int playerId = 0;

	GameLogic logic;

	void Start ()
	{
		logic = GameLogic.CurrentLogic;
	}
	
	void Update ()
	{
		if (logic.GetButtonDown(playerId))
		{
			//Debug.Log("Down");
		}

		if (logic.GetButtonUp(playerId))
		{
			//Debug.Log("Up");
		}

		if (logic.GetButton(playerId))
		{
			//Debug.Log("Hold");
		}
	}
}
