﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class People : MonoBehaviour
{
	[SerializeField]
	Person visualPrefab;
	[SerializeField]
	Transform visualRoot;

    public AudioClip[] audioClips;

    AudioSource audioSource;

	public FaceDescription Desc { get; private set; }

	Person visual;

    public void PlayHookedUpSound() {

        audioSource.PlayOneShot(audioClips[0]);
    }

	public void Init(FaceDescription desc)
	{
		Desc = desc;
		visual = Instantiate(visualPrefab, visualRoot);
		visual.transform.localPosition = Vector3.zero;
		visual.Init(desc, 1f);
	}

	void Start ()
	{
		if (Desc == null)
		{
			Init(FaceDescription.GetRandom());
		}

        audioSource = GetComponent<AudioSource>();

    }
	
	void Update ()
	{
		
	}
}
