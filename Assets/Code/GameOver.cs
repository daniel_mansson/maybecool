﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {
    public Button playButton;
    public Game game;
    public AudioSource buttonSound;
    public AudioClip audioClip;
    public Face player1Face;
    public Face player2Face;
    public Face idealFace;
    public Text player1traits;
    public Text player2traits;
    public Text player1score;
    public Text player2score;
    
    public GameObject player1;
    public GameObject player2;


    private bool player1Wins;
    private bool player2Wins;


    private Vector3 originPosition;
    private Quaternion originRotation;
    private Quaternion shakeRotation;
    public float shake_decay = 0.002f;
    public float shake_intensity = .1f;
    private float temp_shake_intensity = 0;

	// Use this for initialization
	void Start () {

        buttonSound = GetComponent<AudioSource>();

        idealFace.Init(Game.Instance.IdealFace);
        player1Face.Init(Game.Instance.Face1);
        player2Face.Init(Game.Instance.Face2);

        //show acquired traits per players
        var list = FaceDescription.CompareTrait(Game.Instance.Face1, Game.Instance.IdealFace);
        foreach (var item in list)
        {
            player1traits.text += item.type.ToString() + "\n";

        }
        var list2 = FaceDescription.CompareTrait(Game.Instance.Face2, Game.Instance.IdealFace);
        foreach (var item in list2)
        {
            player2traits.text += item.type.ToString() + "\n";

        }

        var score1 = FaceDescription.Compare(Game.Instance.Face1, Game.Instance.IdealFace);
        player1score.text += score1;
        var score2 = FaceDescription.Compare(Game.Instance.Face2, Game.Instance.IdealFace);
        player2score.text += score2;


        if (score1 > score2){
            player1.GetComponent<Animation>().Play();
        }
        else if (score2 > score1){
            player2.GetComponent<Animation>().Play();
        }
        //else
            // Draw


	}

    public void Play() {
        buttonSound.PlayOneShot(buttonSound.clip);
        Game.Instance.GoToMenu();
    }
}

